import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Camera } from '../../../node_modules/@ionic-native/camera';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  foto: string = '';

  constructor(public navCtrl: NavController, private camera: Camera) {

  }

  tiraFoto() {

    this.foto = "";

    this.camera.getPicture({
      saveToPhotoAlbum: true,
      correctOrientation: true,
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
    })
    .then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.foto = base64Image;
    }, (error) => {
      console.log("erro");
    })
    .catch(error => {
      console.log("error");
    })
    
  }

}
